﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy; 
    public Vector2 randomX;
    public Vector2 randomY;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateEnemy", 1, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemy()
    {

        Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
    }
}
