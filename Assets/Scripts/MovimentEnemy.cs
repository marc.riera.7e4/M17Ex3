﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentEnemy : MonoBehaviour
{
    public float speed;    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 target = GameObject.Find("Player").GetComponent<Transform>().position;
        transform.position = Vector2.MoveTowards(transform.position, target, GameObject.Find("Player").GetComponent<DataPlayer>().Speed * Time.deltaTime * speed);
    }
}

