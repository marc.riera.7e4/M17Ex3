﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScore : MonoBehaviour
{
    void Update()
    {
        GameObject.Find("Score").GetComponent<Text>().text = "Score: " + GameManager.Instance.Score;
    }
}
